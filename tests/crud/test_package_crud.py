import unittest
from nose.tools import eq_

from wherecaniget_core.cruds.package_crud import PackageCrud
from tests.persistence.gateways.doubles import PackageGatewayDouble


class PackageCRUDTests(unittest.TestCase):

    def setUp(self):
        self._reset_pk()
        self._reset_packages()
        self.gateway = PackageGatewayDouble()
        self.crud = PackageCrud(self.gateway)

    def test_when_a_packages_is_creatd_it_should_have_an_id(self):
        package = self._create_package()
        self.assertIsNotNone(package.id)

    def test_when_a_package_is_created_its_active_flag_should_be_True(self):
        package = self._create_package()
        eq_(True, package.active)

    def test_should_update_package(self):
        canned_package = self._create_package()
        new_values = dict(name="Carton", id=canned_package.id)
        carton_package = self.crud.save(**new_values)
        eq_(canned_package.id, carton_package.id)
        eq_("Carton", canned_package.name)

    def test_should_retrieve_a_package_with_its_id(self):
        package = self._create_package()
        _package = self.crud.read_by_id(package.id)
        eq_(package.id, _package.id)

    def test_should_retrieve_all_of_packages(self):
        for cnt in range(0, 5):
            self._create_package()
        eq_(5, len(self.crud.get_all_packages()))

    def _create_package(self):
        params = dict(name="Canned", description="12 oz")
        return self.crud.create(**params)

    def _reset_packages(self):
        PackageGatewayDouble.reset_packages()

    def _reset_pk(self):
        PackageGatewayDouble.reset_pk()
