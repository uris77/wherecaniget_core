import unittest
from nose.tools import raises, eq_

from wherecaniget_core.cruds.goods_category_crud import GoodsCategoryCrud
from wherecaniget_core.cruds.goods_item_crud import GoodsItemCrud,\
    GoodsItemCategoryNullError
from tests.persistence.gateways.doubles import GoodsCategoryGatewayDouble,\
    GoodsItemGatewayDouble


class GoodsItemCRUDTests(unittest.TestCase):

    def setUp(self):
        self._reset_categories()
        self._reset_goods()
        self.dairy_category = self._create_category()
        self.gateway = GoodsItemGatewayDouble()
        self.crud = GoodsItemCrud(self.gateway)

    def test_when_a_goods_is_created_it_should_have_an_id(self):
        milk = self._create_goods_item()
        self.assertIsNotNone(milk.id)

    def test_good_should_be_flagged_as_active_when_its_created(self):
        milk = self._create_goods_item()
        eq_(True, milk.active)

    @raises(GoodsItemCategoryNullError)
    def test_should_raise_exception_if_category_is_null(self):
        self.crud.create(**dict(name="Milk"))

    def test_should_not_save_good_if_category_is_null(self):
        try:
            milk = self.crud.create(**dict(name="Milk"))
            self.assertIsNone(milk.id)
        except GoodsItemCategoryNullError as e:
            print("e.strerror: ", e.__str__())

    def test_update_goods_name(self):
        milk = self._create_goods_item()
        params = dict(id=milk.id, name="Chocolate Milk")
        chocolate_milk = self.crud.save(**params)
        eq_(milk.id, chocolate_milk.id)
        eq_("Chocolate Milk", chocolate_milk.name)

    def test_should_retrieve_a_good_with_it_id(self):
        milk = self._create_goods_item()
        _milk = self.crud.read_by_id(milk.id)
        eq_(milk.id, _milk.id)

    def test_should_retrieve_all_goods(self):
        for cnt in range(0, 5):
            self._create_goods_item()
        eq_(5, len(self.crud.get_all_goods()))

    def _create_goods_item(self):
        params = dict(name="Milk", category=self.dairy_category)
        return self.crud.create(**params)

    def _create_category(self):
        gateway = GoodsCategoryGatewayDouble()
        crud = GoodsCategoryCrud(gateway)
        good_category_values = dict(name="Dairy")
        return crud.create(**good_category_values)

    def _reset_categories(self):
        GoodsCategoryGatewayDouble.reset_pk()
        GoodsCategoryGatewayDouble.reset_goods_category()

    def _reset_goods(self):
        GoodsItemGatewayDouble.reset_pk()
        GoodsItemGatewayDouble.reset_goods_items()
