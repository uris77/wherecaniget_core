import unittest
from nose.tools import eq_

from wherecaniget_core.cruds.goods_category_crud import GoodsCategoryCrud
from tests.persistence.gateways.doubles import GoodsCategoryGatewayDouble


class GoodsCategoryCRUDTests(unittest.TestCase):

    def setUp(self):
        GoodsCategoryGatewayDouble.reset_pk()
        GoodsCategoryGatewayDouble.reset_goods_category()
        self.gateway = GoodsCategoryGatewayDouble()
        self.crud = GoodsCategoryCrud(self.gateway)

    def test_when_goods_category_is_saved_it_should_have_an_id(self):
        goods_category = self._create_goods_category()
        self.assertIsNotNone(goods_category.id)

    def test_when_goods_category_should_be_flagged_as_active_when_created(self):
        goods_category = self._create_goods_category()
        eq_(True, goods_category.active)

    def test_should_get_all_categories(self):
        for cnt in range(0, 5):
            self._create_goods_category()
        eq_(5, len(self.crud.get_all_goods_categories()))

    def test_should_retrieve_a_goods_category_with_its_id(self):
        goods_category = self._create_goods_category()
        eq_(goods_category, self.crud.read_by_id(goods_category.id))

    def test_should_update_goods_category(self):
        goods_category = self._create_goods_category()
        update_values = dict(id=goods_category.id, name="Frozen Foods")
        updated_goods_category = self.crud.save(**update_values)
        eq_(goods_category.id, updated_goods_category.id)
        eq_("Frozen Foods", updated_goods_category.name)

    def _create_goods_category(self):
        good_category_values = dict(name="Dairy")
        return self.crud.create(**good_category_values)
