import unittest
from nose.tools import eq_

from wherecaniget_core.cruds.city_crud import CityCrud
from tests.persistence.gateways.doubles import CityGatewayDouble


class CityCRUDTests(unittest.TestCase):

    def setUp(self):
        self._reset_pk()
        self._reset_cities()
        self.gateway = CityGatewayDouble()
        self.crud = CityCrud(self.gateway)

    def test_should_create_a_city(self):
        city = self._create_city()
        self.assertIsNotNone(city.id)

    def test_when_city_is_created_it_is_flagged_as_active(self):
        city = self._create_city()
        eq_(True, city.active)

    def test_retrieve_city_by_id(self):
        city = self._create_city()
        retrieved_city = self.crud.read_by_id(city.id)
        eq_(retrieved_city.id, city.id)

    def test_should_retrieve_all_cities(self):
        for cnt in range(0, 5):
            self._create_city()
        eq_(5, len(self.crud.get_all_cities()))

    def _create_city(self):
        city_values = dict(name="Belmopan")
        return self.crud.create(**city_values)

    def _reset_cities(self):
        CityGatewayDouble.reset_cities()

    def _reset_pk(self):
        CityGatewayDouble.reset_pk()
