import unittest
from nose.tools import eq_

from wherecaniget_core.entities.goods_item import GoodsItem
from tests.persistence.gateways.doubles import GoodsItemGatewayDouble


class GoodsItemCRUDTests(unittest.TestCase):

    def setUp(self):
        self.gateway = GoodsItemGatewayDouble()

    def test_created_item_should_have_an_id(self):
        pass

    def _create_item(self):
        crud = ItemCrud(self.gateway)
