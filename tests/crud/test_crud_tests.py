import unittest
from nose.tools import eq_

from wherecaniget_core.entities.city import City
from wherecaniget_core.cruds.store_crud import StoreCrud
from tests.persistence.gateways.doubles import StoreGatewayDouble


class StoreCRUDTests(unittest.TestCase):

    def setUp(self):
        self._reset_pk()
        self._reset_stores()
        self.gateway = StoreGatewayDouble()
        self.city = City("Belmopan")

    def test_should_create_a_new_store(self):
        store = self._create_store()
        self.assertIsNotNone(store.id)

    def test_store_should_be_flagged_as_active_when_created(self):
        store = self._create_store()
        eq_(True,  store.active)

    def test_should_update_store(self):
        store = self._create_store()
        update_params = dict(name="New Store")
        crud = StoreCrud(self.gateway)
        _store = crud.save(store.id, **update_params)
        eq_(store.id, _store.id)
        eq_("New Store", _store.name)
        eq_(self.city, _store.city)

    def test_should_retrieve_a_store_with_its_id(self):
        store = self._create_store()
        crud = StoreCrud(self.gateway)
        _store = crud.read_by_id(store.id)
        eq_(store, _store)

    def test_should_get_all_stores(self):
        self._create_store()
        self._create_store()
        self._create_store()
        self._create_store()
        crud = StoreCrud(self.gateway)
        stores = crud.get_all_stores()
        eq_(4, len(stores))

    def _create_store(self):
        crud = StoreCrud(self.gateway)
        store_values = dict(name="Store", city=self.city)
        store = crud.create(**store_values)
        return store

    def _reset_pk(self):
        StoreGatewayDouble._pk = 0

    def _reset_stores(self):
        del StoreGatewayDouble._stores[:]
