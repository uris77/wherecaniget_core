from wherecaniget_core.persistence.gateways import StoreGateway,\
    GoodsItemGateway, GoodsCategoryGateway, PackageGateway, CityGateway


class GoodsItemGatewayDouble(GoodsItemGateway):
    __slots__ = ("pk", "item_goods", "persist", "get_all", "get_by_id")

    _item_goods = []
    _pk = 0

    @classmethod
    def reset_pk(cls):
        GoodsItemGatewayDouble._pk = 0

    @classmethod
    def reset_goods_items(cls):
        del GoodsItemGatewayDouble._item_goods[:]

    @property
    def pk(self):
        GoodsItemGatewayDouble._pk += 1
        return GoodsItemGatewayDouble._pk

    @property
    def item_goods(self):
        return GoodsItemGatewayDouble._item_goods

    def get_by_id(self, id):
        for item_good in self.item_goods:
            if item_good.id == id:
                return item_good

    def persist(self, item_good):
        if getattr(item_good, "id", None) is None:
            setattr(item_good, "id", self.pk)
        self.item_goods.append(item_good)
        return item_good

    def get_all(self):
        return [item_good for item_good in self.item_goods if item_good.active is True]


class GoodsCategoryGatewayDouble(GoodsCategoryGateway):
    __slots__ = ("pk", "goods_category", "get_by_id", "persist", "get_all")

    _goods_category = []
    _pk = 0

    @property
    def pk(self):
        GoodsCategoryGatewayDouble._pk += 1
        return GoodsCategoryGatewayDouble._pk

    @property
    def goods_category(self):
        return GoodsCategoryGatewayDouble._goods_category

    @classmethod
    def reset_pk(cls):
        GoodsCategoryGatewayDouble._pk = 0

    @classmethod
    def reset_goods_category(cls):
        del GoodsCategoryGatewayDouble._goods_category[:]

    def get_by_id(self, id):
        for category in self.goods_category:
            if category.id == id:
                return category

    def persist(self, goods_category):
        if getattr(goods_category, "id", None) is None:
            setattr(goods_category, "id", self.pk)
        self.goods_category.append(goods_category)
        return goods_category

    def get_all(self):
        return [goods_category for goods_category in self.goods_category if goods_category.active is True]


class PackageGatewayDouble(PackageGateway):
    __slots__ = ("reset_pk", "pk", "reset_packages", "packages", "persist",
                 "get_all", "get_by_id")

    _packages = []
    _pk = 0

    @classmethod
    def reset_pk(cls):
        PackageGatewayDouble._pk = 0

    @classmethod
    def reset_packages(cls):
        del PackageGatewayDouble._packages[:]

    @property
    def pk(self):
        PackageGatewayDouble._pk += 1
        return PackageGatewayDouble._pk

    @property
    def packages(self):
        return PackageGatewayDouble._packages

    def get_by_id(self, id):
        for package in self.packages:
            if package.id == id:
                return package

    def persist(self, package):
        if getattr(package, "id", None) is None:
            setattr(package, "id", self.pk)
        self.packages.append(package)
        return package

    def get_all(self):
        return [package for package in self.packages if package.active is True]


class StoreGatewayDouble(StoreGateway):
    __slots__ = ("reset_pk", "reset_stores", "pk", "stores", "get_by_id",
                 "persist", "get_all", "retrieve_goods_for_store")
    _stores = []
    _pk = 0

    @classmethod
    def reset_pk(cls):
        StoreGatewayDouble._pk = 0
        CityGatewayDouble._pk = 0

    @classmethod
    def reset_stores(cls):
        del StoreGatewayDouble._stores[:]
        del CityGatewayDouble._cities[:]

    @property
    def pk(self):
        return StoreGatewayDouble._pk

    @property
    def stores(self):
        return StoreGatewayDouble._stores

    def get_by_id(self, id):
        try:
            store = [store for store in self.stores if store.id == id][0]
            return store
        except IndexError:
            return "No Store Exists"

    def persist(self, store):
        if getattr(store, "id", None) is None:
            setattr(store, "id", self.pk)
        self.stores.append(store)
        return store

    def get_all(self):
        return [store for store in self.stores if getattr(store, "active", False) is True]

    def retrieve_goods_for_store(self, store_id):
        store = self.get_by_id(store_id)
        return store.goods


class CityGatewayDouble(CityGateway):
    __slots__ = ("reset_pk", "reset_cites", "pk", "cities", "get_by_id",
                 "persist", "get_all")

    _cities = []
    _pk = 0

    @classmethod
    def reset_pk(cls):
        CityGatewayDouble._pk = 0

    @classmethod
    def reset_cities(cls):
        del CityGatewayDouble._cities[:]

    @property
    def pk(self):
        return CityGatewayDouble._pk

    @property
    def cities(self):
        return CityGatewayDouble._cities

    def get_by_id(self, id):
        try:
            return [city for city in self.cities if city.id == id][0]
        except IndexError:
            return u"City does not exist with this id"

    def persist(self, city):
        if getattr(city, "id", None) is None:
            setattr(city, "id", self.pk)
        setattr(city, "active", True)
        self.cities.append(city)
        return city

    def get_all(self):
        return [city for city in self.cities if city.active is True]

    def find_by_name_like(self, name):
        return [city for city in self.cities if name in city.name]
