from wherecaniget_core.entities.goods_category import GoodsCategory
from wherecaniget_core.entities.package import Package
from wherecaniget_core.entities.goods_item import GoodsItem
from wherecaniget_core.entities.goods_item_package import GoodsItemPackage
from wherecaniget_core.entities.store import Store

from tests.persistence.gateways.doubles import StoreGatewayDouble,\
    GoodsCategoryGatewayDouble, PackageGatewayDouble, GoodsItemGatewayDouble


def make_dairy_category():
    gateway = GoodsCategoryGatewayDouble()
    return gateway.persist(GoodsCategory("Dairy"))


def make_package(name, description):
    gateway = PackageGatewayDouble()
    return gateway.persist(Package(name, description))


def make_canned_packages():
    return make_package("Canned", "Canned Goods")


def make_good(name, category):
    gateway = GoodsItemGatewayDouble()
    return gateway.persist(GoodsItem(name, category))


def make_milk():
    return make_good("Milk", make_dairy_category())


def make_packaged_good(good, package):
    return GoodsItemPackage(good, package)


def make_canned_milk():
    return make_packaged_good(make_milk(), make_canned_packages())


def make_store():
    gateway = StoreGatewayDouble()
    return gateway.persist(Store("Store"))
