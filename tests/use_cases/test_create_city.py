import unittest
from nose.tools import eq_

from tests.persistence.gateways.doubles import CityGatewayDouble
from wherecaniget_core.use_cases.city import create_city


class CreateCityUseCaseTests(unittest.TestCase):

    def setUp(self):
        self.gateway = CityGatewayDouble()

    def tearDown(self):
        CityGatewayDouble.reset_cities()
        CityGatewayDouble.reset_pk()

    def test_should_create_city(self):
        city = self._create_city()
        eq_("Belmopan", city.name)

    def test_city_created_should_have_an_id(self):
        city = self._create_city()
        self.assertIsNotNone(city.id)

    def _create_city(self):
        city_values = dict(name="Belmopan")
        return create_city(self.gateway, **city_values)
