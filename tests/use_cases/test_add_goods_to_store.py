import unittest
from nose.tools import eq_

from wherecaniget_core.entities.store import Store
from wherecaniget_core.use_cases.store_catalog import add_goods_to_store

from tests import factories


class GoodsCatalogAddGoodsToStoreTests(unittest.TestCase):

    def setUp(self):
        self.canned_milk = factories.make_canned_milk()

    def test_adding_a_good_to_a_store_should_increment_its_goods_by_one(self):
        store = factories.make_store()
        add_goods_to_store(store, self.canned_milk, 1.25)
        eq_(1, len(store.goods))

    def test_after_adding_good_to_store_good_sould_be_in_store_goods_list(self):
        store = Store("Store")
        stored_milk = add_goods_to_store(store, self.canned_milk, 1.50)
        self.assertTrue(stored_milk in store.goods)
