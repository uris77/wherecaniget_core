import unittest

from tests.persistence.gateways.doubles import PackageGatewayDouble
from wherecaniget_core.use_cases.package import create_package


class CreatePackageUseCaseTests(unittest.TestCase):

    def setUp(self):
        self.gateway = PackageGatewayDouble()

    def tearDown(self):
        PackageGatewayDouble.reset_packages()
        PackageGatewayDouble.reset_pk()

    def test_created_package_should_have_an_id(self):
        package_values = dict(name="Can", description="Canned Items")
        package = create_package(self.gateway, **package_values)
        self.assertIsNotNone(package.id)
