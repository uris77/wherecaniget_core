import unittest
from nose.tools import eq_

from tests.persistence.gateways.doubles import CityGatewayDouble
from wherecaniget_core.use_cases.city import create_city, update_city


class UpdateCityUseCaseTests(unittest.TestCase):

    def setUp(self):
        self.gateway = CityGatewayDouble()

    def tearDown(self):
        CityGatewayDouble.reset_cities()
        CityGatewayDouble.reset_pk()

    def test_should_update_city_name(self):
        city = self._create_city("Belmopn")
        update_values = dict(name="Belmopan", id=city.id)
        updated_city = update_city(self.gateway, **update_values)
        eq_("Belmopan", updated_city.name)
        eq_(city.id, updated_city.id)

    def _create_city(self, name):
        city_values = dict(name=name)
        return create_city(self.gateway, **city_values)
