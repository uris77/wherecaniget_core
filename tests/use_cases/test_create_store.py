import unittest

from tests.persistence.gateways.doubles import (StoreGatewayDouble,
                                                CityGatewayDouble)
from wherecaniget_core.use_cases.store import create_store
from wherecaniget_core.entities.city import City


class CreateStoreUseCaseTests(unittest.TestCase):

    def setUp(self):
        self.gateway = StoreGatewayDouble()

    def tearDown(self):
        StoreGatewayDouble.reset_stores()
        StoreGatewayDouble.reset_pk()

    def test_should_create_a_store(self):
        city = self._create_city()
        store_values = dict(name="Store", city=city)
        store = create_store(self.gateway, **store_values)
        self.assertIsNotNone(store.id)

    def _create_city(self):
        city_gateway = CityGatewayDouble()
        city = City(name="A City Name")
        return city_gateway.persist(city)
