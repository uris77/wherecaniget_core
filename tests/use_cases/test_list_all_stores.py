import unittest
from nose.tools import eq_

from tests.persistence.gateways.doubles import (StoreGatewayDouble,
                                                CityGatewayDouble)
from wherecaniget_core.use_cases.store import create_store, fetch_stores
from wherecaniget_core.entities.city import City


class ListAllStoresUseCaseTest(unittest.TestCase):

    def setUp(self):
        self.gateway = StoreGatewayDouble()

    def tearDown(self):
        StoreGatewayDouble.reset_stores()
        StoreGatewayDouble.reset_pk()

    def test_if_3_stores_saved_should_fetch_3_stores(self):
        city = self._create_city()
        store1 = create_store(self.gateway, **dict(name=u'Store 1', city=city))
        store2 = create_store(self.gateway, **dict(name=u'Store 2', city=city))
        create_store(self.gateway, **dict(name=u'Store 3', city=city))
        stores = fetch_stores(self.gateway)
        eq_(3, len(stores))
        self.assertIn(store1, stores)
        self.assertIn(store2, stores)

    def _create_city(self):
        city_gateway = CityGatewayDouble()
        city = City("A City Name")
        return city_gateway.persist(city)
