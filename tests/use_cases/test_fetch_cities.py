import unittest
from nose.tools import eq_

from wherecaniget_core.use_cases.city import create_city, fetch_cities
from tests.persistence.gateways.doubles import CityGatewayDouble


class FetchCitiesUseCaseTests(unittest.TestCase):

    def setUp(self):
        self.gateway = CityGatewayDouble()

    def tearDown(self):
        CityGatewayDouble.reset_cities()
        CityGatewayDouble.reset_pk()

    def test_fetch_all_cities(self):
        for cnt in range(0, 5):
            self._create_city("Belmopan")
        eq_(5, len(fetch_cities(self.gateway)))

    def _create_city(self, name):
        city_values = dict(name=name)
        return create_city(self.gateway, **city_values)
