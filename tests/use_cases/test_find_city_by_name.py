import unittest
from nose.tools import eq_

from tests.persistence.gateways.doubles import CityGatewayDouble
from wherecaniget_core.use_cases.city import create_city, find_city_by_name_like


class FindCityByNameUseCaseTests(unittest.TestCase):

    def setUp(self):
        self.gateway = CityGatewayDouble()

    def tearDown(self):
        CityGatewayDouble.reset_cities()
        CityGatewayDouble.reset_pk()

    def test_finding_by_name_should_return_Belize_and_Belmopan(self):
        self._create_cities()
        cities = find_city_by_name_like(u'Bel', self.gateway)
        eq_(2, len(cities))
        self.assertIn(self.belize, cities)
        self.assertIn(self.belmopan, cities)

    def test_finding_name_should_return_Belmopan(self):
        self._create_cities()
        cities = find_city_by_name_like(u'Belmopan', self.gateway)
        eq_(1, len(cities))
        self.assertIn(self.belmopan, cities)

    def _create_cities(self):
        self.belize = create_city(self.gateway, **dict(name="Belize"))
        self.belmopan = create_city(self.gateway, **dict(name="Belmopan"))
        create_city(self.gateway, **dict(name="Benque"))
        create_city(self.gateway, **dict(name="Berlin"))
        create_city(self.gateway, **dict(name="Dangriga"))
