from setuptools import setup, find_packages
import sys, os

version = '0.0.1'

setup(name='wherecaniget_core',
      version=version,
      description="Where Can I Get?",
      long_description="""\
Voter Management""",
      classifiers=[], # Get strings from http://pypi.python.org/pypi?%3Aaction=list_classifiers
      keywords='voter',
      author='Roberto Guerra',
      author_email='uris77@gmail.com',
      url='',
      license='Apache',
      packages=find_packages(exclude=['ez_setup', 'examples', 'tests']),
      include_package_data=True,
      zip_safe=False,
      test_suite="nose.collector",
      testpkgs=['nose', 'coverage'],
      install_requires=[
          'nose'
          # -*- Extra requirements: -*-
      ],
      entry_points="""
      # -*- Entry points: -*-
      """,
      )
