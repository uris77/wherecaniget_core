from wherecaniget_core.cruds.package_crud import PackageCrud


def create_package(gateway, **kwargs):
    crud = PackageCrud(gateway)
    package = crud.create(**kwargs)
    return package.__tuple__
