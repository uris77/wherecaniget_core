from wherecaniget_core.cruds.store_crud import StoreCrud


def create_store(gateway, **kwargs):
    crud = StoreCrud(gateway)
    store = crud.create(**kwargs)
    return store.__tuple__


def fetch_stores(gateway):
    crud = StoreCrud(gateway)
    return [store.__tuple__ for store in crud.get_all_stores()]
