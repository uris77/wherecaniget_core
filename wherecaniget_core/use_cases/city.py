from wherecaniget_core.cruds.city_crud import CityCrud


def create_city(gateway, **kwargs):
    crud = CityCrud(gateway)
    city = crud.create(**kwargs)
    return city.__tuple__


def fetch_cities(gateway):
    crud = CityCrud(gateway)
    return [city.__tuple__ for city in crud.get_all_cities()]


def update_city(gateway, **kwargs):
    crud = CityCrud(gateway)
    city = crud.read_by_id(kwargs['id'])
    city.name = kwargs['name']
    city = gateway.persist(city)
    return city.__tuple__


def find_city_by_name_like(city_name, gateway):
    cities = gateway.find_by_name_like(city_name)
    return [city.__tuple__ for city in cities]
