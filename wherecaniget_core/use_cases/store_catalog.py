from wherecaniget_core.entities.store_item import StoreItem


def add_goods_to_store(store, packaged_good, price):
    store_good = StoreItem(store, packaged_good, price)
    if getattr(store, "goods", None) is None:
        setattr(store, "goods", [])
    store.goods.append(store_good)
    return store_good
