import abc


class StoreGateway(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractproperty
    def get_by_id(self, id):
        """Get an instance of Store with the specified id"""
        raise NotImplementedError("Subclass should implement get_by_id(id)!")

    @abc.abstractproperty
    def persist(self, entity):
        """Persist an instance to the underlying data store"""
        raise NotImplementedError("Subclass should implement persist(entity)!")

    @abc.abstractproperty
    def get_all(self):
        """Retrieve all the store instances"""
        raise NotImplementedError("Subclass should implement list()")

    @abc.abstractproperty
    def retrieve_goods_for_store(self, store_id):
        """Retrieves all goods for a store with the givein id"""
        raise NotImplementedError("Subclass should implement retrieve_goods_for_store(store_id)!")


class GoodsItemGateway(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractproperty
    def get_by_id(self, id):
        """Retrieve an instance of GoodsItem with the specified id"""
        raise NotImplementedError("Subclass should implement get_by_id(id)!")

    @abc.abstractproperty
    def persist(self, goods_item):
        """Persist an instance of GoodsItem to the underlying data store"""
        raise NotImplementedError("Subclass should implement persist(goods_item)!")

    @abc.abstractproperty
    def get_all(self):
        """Retrieve all the items"""
        raise NotImplementedError("Subclass should implement list()!")


class PackageGateway(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractproperty
    def get_by_id(self, id):
        """Retrieve an instanve of Package with the specified id"""
        raise NotImplementedError("Subclass should implement get_by_id(id)!")

    @abc.abstractproperty
    def persist(self, package):
        """Persist an instance of Package"""
        raise NotImplementedError("Subclass should implement persist(package)!")

    @abc.abstractproperty
    def get_all(self):
        """Retrieve all the packages"""
        raise NotImplementedError("Subclass should implement get_all()!")


class GoodsItemPackageGateway(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractproperty
    def get_by_id(self, id):
        """Retrieve an instance of GoodsItemPackage with the specified id"""
        raise NotImplementedError("Subclass should implement get_by_id(id)!")

    @abc.abstractproperty
    def persist(self, goods_item_package):
        """Persist an instance of GoodsItemPackage"""
        raise NotImplementedError("Subclass should implement persist(goods_item_package)!")

    @abc.abstractproperty
    def get_all(self):
        """Retrieve all the item packages"""
        raise NotImplementedError("Subclass should implement get_all()!")


class StoreGoodsItemGateway(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractproperty
    def get_by_id(self, id):
        """Retrieve an instance of StoreGoodsItem with the specified id"""
        raise NotImplementedError("Subclass should implement get_by_id(id)!")

    @abc.abstractproperty
    def persist(self, store_goods_item):
        """Persist an instance of StoreItem"""
        raise NotImplementedError("Subclass should implement persist(store_goods_item)!")

    @abc.abstractproperty
    def get_all(self):
        """Retrieve all the instances of StoreGoodsItem"""
        raise NotImplementedError("Subclass should implement get_all()!")


class GoodsCategoryGateway(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractproperty
    def get_by_id(self, id):
        """Retrieve an instance of GoodsCategory with the specified id"""
        raise NotImplementedError("Subclass shoud implement get_by_id(id)!")

    @abc.abstractproperty
    def persist(self, goods_category):
        """Persist an instance of GoodsCategory"""
        raise NotImplementedError("Subclass should implement persist(goods_category)!")

    @abc.abstractproperty
    def get_all(self):
        """Retrieve all the instance of GoodsCategory"""
        raise NotImplementedError("Subclass should implement get_all()!")


class CityGateway(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractproperty
    def get_by_id(self, id):
        """Retrieve an instance of City with the specified id"""
        raise NotImplementedError("Subclass should implement get_by_id(id)!")

    @abc.abstractproperty
    def persist(self, city):
        """Persist an instance of City"""
        raise NotImplementedError("Subclass should implement persist(city)!")

    @abc.abstractproperty
    def get_all(self):
        """Retrieves all cities"""
        raise NotImplementedError("Subclass should implement get_all()!")

    @abc.abstractproperty
    def find_by_name_like(self, city_name):
        """Retrieves all cities whoce name begin with city_name"""
        raise NotImplementedError("Subclass should implement find_by_name_like(city_name)!")
