from wherecaniget_core.entities.goods_item import GoodsItem


class GoodsItemCrud(object):

    __slots__ = ("gateway")

    def __init__(self, gateway):
        self.gateway = gateway

    def create(self, **kwargs):
        if kwargs.get('category', None) is None:
            raise GoodsItemCategoryNullError("GoodsItem must have a non-nullable Category!")
        goods_item = GoodsItem(kwargs['name'], kwargs['category'])
        setattr(goods_item, "active", True)
        return self.gateway.persist(goods_item)

    def save(self, **kwargs):
        goods_item = self.gateway.get_by_id(kwargs.get("id"))
        goods_item.name = kwargs.get("name", goods_item.name)
        return self.gateway.persist(goods_item)

    def read_by_id(self, id):
        return self.gateway.get_by_id(id)

    def get_all_goods(self):
        return self.gateway.get_all()


class GoodsItemCategoryNullError(Exception):

    def __init__(self, message):
        self.message = message

    def __str__(self):
        return repr(self.message)
