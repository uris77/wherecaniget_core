from wherecaniget_core.entities.city import City


class CityCrud(object):

    __slots__ = ('create', 'read_by_id', 'get_all_cities', 'gateway')

    def __init__(self, gateway):
        self.gateway = gateway

    def create(self, **kwargs):
        city = City(name=kwargs['name'])
        return self.gateway.persist(city)

    def read_by_id(self, id):
        return self.gateway.get_by_id(id)

    def get_all_cities(self):
        return self.gateway.get_all()
