from wherecaniget_core.entities.goods_category import GoodsCategory


class GoodsCategoryCrud(object):

    __slots__ = ("gateway")

    def __init__(self, gateway):
        self.gateway = gateway

    def create(self, **kwargs):
        goods_category = GoodsCategory(kwargs['name'])
        setattr(goods_category, "active", True)
        return self.gateway.persist(goods_category)

    def save(self, **kwargs):
        goods_category = self.gateway.get_by_id(int(kwargs.get('id')))
        goods_category.name = kwargs.get("name", goods_category.name)
        return self.gateway.persist(goods_category)

    def get_all_goods_categories(self):
        return self.gateway.get_all()

    def read_by_id(self, id):
        return self.gateway.get_by_id(id)
