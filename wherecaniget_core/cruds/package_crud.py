from wherecaniget_core.entities.package import Package


class PackageCrud(object):

    def __init__(self, gateway):
        self.gateway = gateway

    def create(self, **kwargs):
        package = Package(kwargs["name"], kwargs["description"])
        setattr(package, "active", True)
        return self.gateway.persist(package)

    def save(self, **kwargs):
        package = self.gateway.get_by_id(kwargs["id"])
        package.name = kwargs.get("name", package.name)
        package.description = kwargs.get("description", package.description)
        return self.gateway.persist(package)

    def read_by_id(self, id):
        return self.gateway.get_by_id(id)

    def get_all_packages(self):
        return self.gateway.get_all()
