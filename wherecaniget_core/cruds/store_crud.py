from wherecaniget_core.entities.store import Store


class StoreCrud(object):
    __slots__ = ("create", "save", "read_by_id", "get_all_stores", "gateway")

    def __init__(self, gateway):
        self.gateway = gateway

    def create(self, **kwargs):
        store = Store(kwargs["name"], kwargs["city"])
        setattr(store, "active", True)
        return self.gateway.persist(store)

    def save(self, store_id, **kwargs):
        store = self.gateway.get_by_id(store_id)
        store.name = kwargs.get("name", store.name)
        store.city = kwargs.get("city", store.city)
        return self.gateway.persist(store)

    def read_by_id(self, id):
        store = self.gateway.get_by_id(id)
        return store

    def get_all_stores(self):
        return self.gateway.get_all()
