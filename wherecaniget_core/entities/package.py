from collections import namedtuple


PackageTuple = namedtuple("PackageTuple", ("id name description"))


class Package(object):

    def __init__(self, name, description):
        self.name = name
        self.description = description

    @property
    def __tuple__(self):
        return PackageTuple(
            id=getattr(self, "id", None),
            name=self.name,
            description=self.description)
