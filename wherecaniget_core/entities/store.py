from collections import namedtuple


StoreTuple = namedtuple("StoreTuple", ("id name city active"))


class Store(object):

    def __init__(self, name, city=None):
        self.name = name
        self.city = city

    @property
    def goods(self):
        if getattr(self, "_goods", None) is None:
            setattr(self, "_goods", [])
        return self._goods

    @property
    def __tuple__(self):
        city_tuple = self.city.__tuple__
        store_tuple = StoreTuple(id=getattr(self, "id", None),
                                 name=getattr(self, "name", None),
                                 city=city_tuple,
                                 active=getattr(self, "active", None))
        return store_tuple
