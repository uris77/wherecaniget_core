from collections import namedtuple


CityTuple = namedtuple('CityTuple', 'id name latitude longitude')


class City(object):

    def __init__(self, name, latitude=None, longitude=None):
        self.name = name
        self.latitude = None
        self.longitude = longitude

    @property
    def __tuple__(self):
        city_tuple = CityTuple(id=getattr(self, "id", None),
                               name=getattr(self, "name", None),
                               latitude=getattr(self, "latitude", None),
                               longitude=getattr(self, "longitude", None)
                               )
        return city_tuple
